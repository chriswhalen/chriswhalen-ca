#!/bin/sh

service nginx start
certbot --nginx -n -d chriswhalen.ca --agree-tos --email "chris@chriswhalen.ca"

sleep infinity
