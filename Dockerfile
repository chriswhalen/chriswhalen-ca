FROM nginx

COPY nginx.conf /etc/nginx/nginx.conf
COPY start.sh /opt

ADD www/ /usr/share/nginx/www/
RUN chmod -R a+r /usr/share/nginx/www

RUN apt update
RUN apt install -y certbot python-certbot-nginx

EXPOSE 80 443

ENTRYPOINT /opt/start.sh
