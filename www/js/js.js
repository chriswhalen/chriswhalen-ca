
if (window.location.protocol != 'https:' || window.location.hostname != 'chriswhalen.ca') window.location.href = "https://chriswhalen.ca";

dyeTime = 2;
saturation = 32;
width = 0;

function rgbrand(a) {

    a = typeof a !== 'undefined' ? a : 1;

    var rgb = Please.make_color({
    	//golden: false,
    	//saturation: Math.max(Math.random()+0.24, 0.72),
        format: 'rgb',
    })[0];

    rgb.r < 128 ? rgb.r = Math.max(rgb.r-saturation, 0) : Math.min(rgb.r+saturation, 255)
    rgb.g < 128 ? rgb.g = Math.max(rgb.g-saturation, 0) : Math.min(rgb.g+saturation, 255)
    rgb.b < 128 ? rgb.b = Math.max(rgb.b-saturation, 0) : Math.min(rgb.b+saturation, 255)

    return 'rgba('+rgb.r+','+rgb.g+','+rgb.b+','+a+')';
}


function dye(one) {

    if (one) {
        var t = _.flatten(_(96).times(function(){ return 0 } ));
    }
    else {
        var t = _.shuffle(  _.flatten(_(dyeTime + 2).times(function(){ return _.range(400, 2800, 400) } )));
    }

    $('#front em b').each(function(i,e){
        setTimeout(function() { $(e).css('color', rgbrand()) }, _.reduce(_.flatten(_(dyeTime).times(function(){return t.pop()})),function(m,n){return m+n;},0) );
    });

    $('#back b').each(function(i,e){
        setTimeout(function() {
            $(e).css('color', rgbrand(0.5));
            setTimeout(function() { $(e).css('color', rgbrand()) }, t.pop());
        }, t.pop());

    });

    setTimeout(dye, 4000);
}


function fit() {

    if ($('html').width() < 768 ) {
        $('#name').css('zoom', $(window).width()/8+'%' )
    }
    else if (width < 768) {
        $('#name').css('zoom', '100%' )
    }

    width = $('html').width();
    window.dx = $(window).width() / 2;
    window.dy = $(window).height() / 2;
}


function reposition(e) {

    if (e) {
        var x = window.dx - e.pageX;
        var y = window.dy - e.pageY;
    } else {
        var x = 0;
        var y = 0;
    }

    var theta = Math.atan2(y,x);
    var xx =  Math.cos(theta)*Math.sqrt(Math.pow(x,2) + Math.pow(y,2))
    var yy =  Math.sin(theta)*Math.sqrt(Math.pow(x,2) + Math.pow(y,2))

    xx = window.dx / ( window.dx / (Math.round(xx, 0)+0.5) )/4;
    yy = window.dy / ( window.dy / (Math.round(yy, 0)+0.5) )/4;

    $('#front').css('transform', 'translate('+xx+'px, '+yy+'px) scale('+ (((Math.abs(xx)+Math.abs(yy))/1000)+1) +')');
    $('#back').css('transform', 'translate('+(-xx/2)+'px, '+(-yy/2)+'px) scale('+ (1-(((Math.abs(xx)+Math.abs(yy))/1000))) +')').css('opacity', (1-(((Math.abs(xx)+Math.abs(yy))/1000))) );
}


$(function(){

    dye(1);
    fit();

    $(window).on('resize', fit)

    $(window).on('orientationchange', function(){
        $('#name').css('position', 'absolute');
        setTimeout( function() {
            $('#name').css('position', 'relative')
            reposition();
        }, 200)
    })

    $('#name').animate({'opacity':0.12} , { queue: true, duration: 150, complete: function() {
            $('#name').animate({'opacity':1}, {queue: false, duration: 800});
            $('#whalen').css({'opacity':1});
            $('#name i').css({'opacity':0.16});
        }}
    );

    setTimeout(function(){
        $('body').mousemove( function(e){
            reposition(e);
        })
        $(body).addClass('started');
    }, 1200);
});
